<?php include "includes/admin_header.php"?>

<?php 

    if(isset($_SESSION['username'])){
        $username = $_SESSION['username'];
        $query = "SELECT * FROM users WHERE username = '{$username}' ";
        $select_user_profile_query = mysqli_query($connection, $query);
        while($row = mysqli_fetch_array($select_user_profile_query)){
            $user_id = $row['user_id'];
            $user_firstname = $row['user_firstname'];
            $user_lastname = $row['user_lastname'];
            $username = $row['username'];
            $db_user_password = $row['user_password'];
            $user_email = $row['user_email'];
            $user_image = $row['user_image'];
            $user_role = $row['user_role'];
        }
    }

    ?>

    <?php 

    $message = '';
    if(isset($_POST['edit_user'])){
        $username = $_POST['username'];
        $user_firstname = $_POST['user_firstname'];
        $user_lastname = $_POST['user_lastname'];
        $user_image = $_FILES['image']['name'];
        $user_image_temp = $_FILES['image']['tmp_name'];
        $user_email = $_POST['user_email'];
        $user_role = $_POST['user_role'];
        $user_password = $_POST['user_password'];

        if(empty($user_password)){
            $message = "<div class='alert alert-danger' role='alert'>Please update password or fill current one!</div>";
        } else {       
            move_uploaded_file($user_image_temp, "../images/$user_image");

        if(empty($post_image)){
            $query = "SELECT * FROM users WHERE username = $username";
            $select_image = mysqli_query($connection,$query);
            while($row = mysqli_fetch_assoc($select_image))
            $post_image = $row['post_image'];
        }

        if($db_user_password != $user_password) {
            $hashed_password = password_hash($user_password, PASSWORD_BCRYPT, array('cost' => 10));
        } 

        $query = "UPDATE users SET ";
        $query .= "username = '{$username}', ";
        $query .= "user_firstname = '{$user_firstname}', ";
        $query .= "user_lastname = '{$user_lastname}', ";
        $query .= "user_email = '{$user_email}', ";
        $query .= "user_role = '{$user_role}', ";
        $query .= "user_image = '{$user_image}', ";
        $query .= "user_password = '{$hashed_password}' ";
        $query .= "WHERE username = '{$username}' ";

        $edit_user_query = mysqli_query($connection, $query);
        confirmQuery($edit_user_query);
        $message = "<div class='alert alert-success' role='alert'>Profile Updated Successfully!</div>";
      }
    } 
    
    ?>

<body>
    <div id="wrapper">

        <!-- Navigation -->
        <?php include "includes/admin_navigation.php"?>        

        <div id="page-wrapper">
            <div class="container-fluid">

            <?php echo $message;?>
            
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <small>Role is: <?php echo $_SESSION['user_role'];?></small>Welcome to Dashboard 
                            <?php echo strtoupper(get_user_name()); ?>
                        </h1>
                        <form action="" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="user_firstname">First Name</label>
                                <input value="<?php echo $user_firstname; ?>" class="form-control" name="user_firstname" type="text">
                            </div>
                            <div class="form-group">
                                <label for="user_lastname">Last Name</label>
                                <input class="form-control" value="<?php echo $user_lastname; ?>" name="user_lastname" type="text">
                            </div>

                            <?php 
                            
                            if(is_admin()){ ?>

                                <div class="form-group">
                                <select name="user_role" id="">
                                    <option value="<?php echo $user_role; ?>"><?php echo $user_role; ?></option>
                        
                                    <?php

                                        if(is_admin()){
                                            echo "<option value='subscriber'>subscriber</option>";
                                        } else {
                                            echo " <option value='admin'>admin</option>";
                                        }
                                    } ?>
                        
                                </select>
                            </div>

                            <div class="form-group">
                                <img style="border-radius: 50%; " width='100' src="../images/<?php echo $user_image; ?>">
                                <input type="file" name="image">
                            </div>
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input class="form-control" value="<?php echo $username ?>" name="username" type="text">
                            </div>
                            <div class="form-group">
                                <label for="user_email">User Email</label>
                                <input class="form-control" value="<?php echo $user_email ?>" name="user_email" type="email">
                            </div>
                            <div class="form-group">
                                <label for="user_password">Password</label>
                                <input autocomplete="off" class="form-control" name="user_password" type="password">
                            </div>
                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" name="edit_user" value="Update Profile" >
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include "includes/admin_footer.php"?>
   
