<?php  include "includes/db.php"; ?>
 <?php  include "includes/header.php"; ?>
 <?php include "admin/functions.php";?>
 

<!-- Setting Language Variables -->

<?php

// if(isset($_GET['lang']) && !empty($_GET['lang'])) {

//     $_SESSION['lang'] = $_GET['lang'];

//     if(isset($_SESSION['lang']) && $_SESSION['lang'] != $_GET['lang']){

//         echo "<script type='text/javascript'> location.reload(); </script>";
//     }
// }

// if(isset($_SESSION['lang'])){ 

//     include "includes/languages/".$_SESSION['lang'].".php";
// } else { 

//     include "includes/languages/en.php";
// }


?>

 <?php 
 
   if($_SERVER['REQUEST_METHOD'] == "POST") {

    $username = trim($_POST['username']);
    $user_email = trim($_POST['email']);
    $user_password = trim($_POST['password']);

    $error = [

        'username'=>'',
        'email'=>'',
        'password'=>''

    ];

    if(strlen($username) < 4){

        $error['username'] = "<div class='alert alert-danger' role='alert'>Username should be longer than 4 character!</div>";
    }

    if($username == ''){

        $error['username'] = "<div class='alert alert-danger' role='alert'>Username can't be empty</div>";
    }

    if(username_exist($username)){

        $error['username'] = "<div class='alert alert-danger' role='alert'>Username already exists</div>";
    }

    if($user_email == ''){

        $error['email'] = "<div class='alert alert-danger' role='alert'>Email can't be empty";
    }

    if(username_exist($user_email)){

        $error['email'] = "<div class='alert alert-danger' role='alert'>Email already exists,<a href='index.php'>Please Login</a></div>";
    }

    if($user_password == ''){

        $error['password'] = "<div class='alert alert-danger' role='alert'>Password can't be empty</div>";
    }

    foreach($error as $key => $value){

        if(empty($value)){ 

            unset($error[$key]);

        }
    }

    if(empty($error)){

        user_register($username,$user_email,$user_password);
        user_login($username,$user_password);
    }


} 
 

 ?>


    <!-- Navigation -->
    
    <?php  include "includes/navigation.php"; ?>
    
 
    <!-- Page Content -->
    <div class="container">

    <!-- <form method="get" class="navbar-form navbar-right" action="" id="language_form">
        <div class="form-group">
            <select name="lang" class="form-control" onchange="changeLanguage()">
                <option value="en" <?php if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'en'){echo "selected"; } ?> >English</option>
                <option value="ru" <?php if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'ru'){echo "selected"; } ?>>Russian</option>
            </select>
        </div>
    
    </form> -->
    
<section id="login">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3">
                <div class="form-wrap">
                <h1>Registration</h1>
                    <form role="form" action="" method="post" id="login-form" autocomplete="off">
                        <div class="form-group">
                            <label for="username" class="sr-only">username</label>
                            <input value="<?php echo isset($username) ? $username: ''?>" type="text" name="username" id="username" class="form-control" placeholder="Enter Desired Username" autocomplete="on">
                            <p><?php echo isset($error['username']) ? $error['username']: ''?></p>
                        </div>
                         <div class="form-group">
                            <label for="email" class="sr-only">Email</label>
                            <input value="<?php echo isset($user_email) ? $user_email: ''?>" type="email" name="email" id="email" class="form-control" placeholder="somebody@example.com" autocomplete="on">
                            <p><?php echo isset($error['email']) ? $error['email']: ''?></p>
                        </div>
                         <div class="form-group">
                            <label for="password" class="sr-only">Password</label>
                            <input type="password" name="password" id="key" class="form-control" placeholder="Password">
                            <p><?php echo isset($error['password']) ? $error['password']: ''?></p>
                        </div>
                
                        <input type="submit" name="register" id="btn-login" class="btn btn-primary btn-lg btn-block" value="Register">
                    </form>
                 
                </div>
            </div> <!-- /.col-xs-12 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section>

        <hr>

        <!-- <script>
            function changeLanguage(){

                document.getElementById("language_form").submit();
                // console.log('dadda');
            }
        </script> -->

<?php include "includes/footer.php";?>
