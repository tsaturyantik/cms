<?php

if(isset($_POST['checkboxArray'])){
    foreach($_POST['checkboxArray'] as $postValueId){
        $bulkOptions = $_POST['bulkOptions'];
        switch($bulkOptions){
            case 'published':
            case 'draft':

        $query = "UPDATE posts SET post_status = '{$bulkOptions}' WHERE post_id = {$postValueId} ";
        $update_to_change_status = mysqli_query($connection,$query);
        confirmQuery($update_to_change_status );

            break;
            case 'delete':

        $query = "DELETE FROM posts WHERE post_id = {$postValueId} ";
        $delete_post_query = mysqli_query($connection,$query);
        confirmQuery($delete_post_query );

            break;
            case 'clone';

            $query = "SELECT * FROM posts WHERE post_id = '{$postValueId}' ";
            $select_post_query = mysqli_query($connection,$query);
            confirmQuery($select_post_query);
            
            while($row = mysqli_fetch_array($select_post_query)){
                $post_category_id = $row['post_category_id'];
                $post_title = mysqli_real_escape_string($connection,$row['post_title']);
                $post_user_id = $row['user_id'];
                $post_user = $row['post_user'];
                $post_date = $row['post_date'];
                $post_image = $row['post_image'];
                $post_content = mysqli_real_escape_string($connection,substr($row['post_content'], 0, 30));
                $post_tags = mysqli_real_escape_string($connection,$row['post_tags']);
                $post_status = $row['post_status'];
            }

            $query = "INSERT INTO posts(post_category_id,post_title,user_id,post_user,post_date,post_image,post_content,post_tags,post_status) ";
            $query .= "VALUE({$post_category_id},'{$post_title}', '{$post_user_id}','{$post_user}', now(), '{$post_image}', '{$post_content}','{$post_tags}','{$post_status}' )";
            $copy_query = mysqli_query($connection,$query);
            confirmQuery($copy_query);
            echo "<div class='alert alert-success' role='alert'>Post Cloned Successfully!</div>";

            break;
            case 'reset_views';

            $query = "UPDATE posts SET post_views_count = 0 WHERE post_id = {$postValueId} ";
            $reset_post_views_count = mysqli_query($connection,$query);
            confirmQuery($reset_post_views_count);

            break;
        }   
    }
}

?>

<form action="" method="post">
    <table class="table table-bordered table-hover">
        <div id="bulkOptionContainer" style="padding: 0px;" class="col-xs-4">
            <select class="form-control" name="bulkOptions" id="">
                <option value="">Select Option</option>
                <option value="published">Publish</option>
                <option value="draft">Draft</option>
                <option value="clone">Clone</option>
                <option value="reset_views">Reset Views</option>
                <option value="delete">Delete</option>
            </select>
        </div>
        <div class="col-xs-4">
            <input type="submit" name="submit" class="btn btn-success" value="Apply">
            <a class="btn btn-primary" href="posts.php?source=add_post">Add New</a>
        </div>
        <thead>
            <tr>
                <th><input type="checkbox" id="selectAllBoxes"></th>
                <th>Id</th>
                <th>User</th>
                <th>Title</th>
                <th>Content</th>
                <th>Category</th>
                <th>Comments</th>
                <th>Tags</th>
                <th>Image</th>
                <th>Status</th>
                <th>Date</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
                <th>Views</th>
            </tr>
        </thead>
            <tbody>
            <?php 

            if(is_admin()){
                $query = "SELECT posts.post_id,posts.post_category_id,posts.post_title,posts.post_user,posts.post_date,posts.post_image,posts.post_content,posts.post_tags, ";
                $query .= "posts.post_comment_count,posts.post_status,posts.post_views_count,categories.cat_id,categories.cat_title ";
                $query .= " FROM posts ";
                $query .= " LEFT JOIN categories ON posts.post_category_id = categories.cat_id ORDER BY posts.post_id DESC ";  
            } else {
                $user = currentUser();
                $query = "SELECT posts.post_id,posts.post_category_id,posts.post_title,posts.post_user,posts.post_date,posts.post_image,posts.post_content,posts.post_tags, ";
                $query .= "posts.post_comment_count,posts.post_status,posts.post_views_count,categories.cat_id,categories.cat_title ";
                $query .= " FROM posts ";
                $query .= " LEFT JOIN categories ON posts.post_category_id = categories.cat_id WHERE posts.post_user = '$user' ORDER BY posts.post_id DESC ";
            }

            $select_posts = mysqli_query($connection, $query);
                while($row = mysqli_fetch_assoc($select_posts)){
                $post_id = $row['post_id'];
                $post_category_id = $row['post_category_id'];
                $post_title = $row['post_title'];
                $post_user = $row['post_user'];
                $post_date = $row['post_date'];
                $post_image = $row['post_image'];
                $post_content = substr($row['post_content'], 0, 30);
                $post_tags = $row['post_tags'];
                $post_comment_count = $row['post_comment_count'];
                $post_status = $row['post_status'];
                $post_views_count = $row['post_views_count'];
                $cat_id = $row['cat_id'];
                $cat_title = $row['cat_title'];

                echo "<tr>";
                
                ?>

                <td><input type="checkbox" class="checkBoxes" name="checkboxArray[]" value="<?php echo $post_id; ?>"></td>

                <?php

                echo "<td>$post_id</td>";

                if(isset($post_user) || !empty($post_user)){
                    echo "<td>$post_user</td>"; 
                }

                echo "<td>$post_title</td>";
                echo "<td>$post_content</td>";
                echo "<td>$cat_title</td>";

                $query = "SELECT * FROM comments WHERE comment_post_id = $post_id";
                $send_comment_query = mysqli_query($connection, $query);
                $row = mysqli_fetch_array($send_comment_query);
                $comment_id = $row['comment_id'];
                $comment_count = mysqli_num_rows($send_comment_query);

                echo "<td><a href='post_comments.php?id=$post_id'>$comment_count</a></td>";
                echo "<td>$post_tags</td>";
                echo "<td><img width='100' src='../images/$post_image' alt='image'></td>";
                echo "<td>$post_status</td>";
                echo "<td>$post_date</td>";     
                echo "<td><a class='btn btn-primary' href='../post.php?p_id=$post_id'>View</a></td>";
                echo "<td><a class='btn btn-info' href='posts.php?source=edit_post&p_id={$post_id}'>Edit</a></td>";

                ?>

                <form method="post" action="">
                    <input type="hidden" name="post_id" value="<?php echo $post_id ?>">
                <?php

                echo '<td><input class="btn btn-danger" type="submit" name="delete" value="Delete"></td>';

                ?>

                </form>

                <?php
                
                echo "<td>$post_views_count</td>";
                echo "</tr>";

            }
            
            ?>

        </tbody>
    </table>
</form>  

<?php

if(isset($_POST['delete'])){
    $the_post_id = $_POST['post_id'];
    $query = "DELETE FROM posts WHERE post_id = $the_post_id";
    $delete_post_query = mysqli_query($connection, $query);
    confirmQuery($delete_post_query);
    header("Location: posts.php");
}

?>