<?php 

    if(isset($_GET['edit_user'])){

        $the_user_id = $_GET['edit_user'];
        $query = "SELECT * FROM users WHERE user_id = $the_user_id";
        $select_users_query = mysqli_query($connection, $query);
    
            while($row = mysqli_fetch_assoc($select_users_query)){
            
            $user_id = $row['user_id'];
            $user_firstname = $row['user_firstname'];
            $user_lastname = $row['user_lastname'];
            $username = $row['username'];
            $user_password = $row['user_password'];
            $user_email = $row['user_email'];
            $user_image = $row['user_image'];
            $user_role = $row['user_role'];
            
        } 

    
    if(isset($_POST['edit_user'])){
        
        $username = mysqli_real_escape_string($connection,$_POST['username']);
        $user_firstname = mysqli_real_escape_string($connection,$_POST['user_firstname']);
        $user_lastname = mysqli_real_escape_string($connection,$_POST['user_lastname']);

        // $user_image = $_FILES['image']['name'];
        // $user_image_temp = $_FILES['image']['tmp_name'];

        $user_email = $_POST['user_email'];
        $user_password = $_POST['user_password'];
        $user_role = $_POST['user_role'];
        // $user_date = date('d-m-y');
        

        // move_uploaded_file($user_image_temp, "../images/$user_image");

        if(!empty($user_password)){

            $query_password = "SELECT user_password FROM users WHERE user_id = $the_user_id";
            $get_user_query = mysqli_query($connection, $query_password);
            confirmQuery($get_user_query);

            $row = mysqli_fetch_array($get_user_query);
            $db_user_password = $row['user_password'];

            if($db_user_password != $user_password) {
                $hashed_password = password_hash($user_password, PASSWORD_BCRYPT, array('cost' => 10));
            } 

        $query = "UPDATE users SET ";
        $query .= "username = '{$username}', ";
        $query .= "user_firstname = '{$user_firstname}', ";
        $query .= "user_lastname = '{$user_lastname}', ";
        $query .= "user_email = '{$user_email}', ";
        $query .= "user_password = '{$hashed_password}', ";
        $query .= "user_role = '{$user_role}' ";
        $query .= "WHERE user_id = '{$the_user_id}' ";
        
        $edit_user_query = mysqli_query($connection, $query);
        confirmQuery($edit_user_query);
        echo "<div class='alert alert-success' role='alert'>User Updated Successfully!</div>";
        echo "<h4><a href='users.php'>View Users</a></h4>" ;
        
        } else {

            echo "<div class='alert alert-danger' role='alert'>Please update password or fill current one!</div>";
        }

    }

} else {

    header("Location: index.php");
}

?>                              

<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="user_firstname">First Name</label>
        <input value="<?php echo $user_firstname ?>" class="form-control" name="user_firstname" type="text">
    </div>
    <div class="form-group">
        <label for="user_lastname">Last Name</label>
        <input class="form-control" value="<?php echo $user_lastname ?>" name="user_lastname" type="text">
    </div>
    <div class="form-group">
        <select name="user_role" id="">
        <option value="<?php echo $user_role; ?>"><?php echo $user_role; ?></option>

        <?php

        if($user_role == 'admin'){
            echo "<option value='subscriber'>subscriber</option>";

        } else {
            echo " <option value='admin'>admin</option>";

        }


        ?>
            
        
        </select>
    </div>
    <div class="form-group">
        <label for="user_image">User Image</label>
        <input type="file" name="image">
    </div>
    <div class="form-group">
        <label for="username">Username</label>
        <input class="form-control" value="<?php echo $username ?>" name="username" type="text">
    </div>
    <div class="form-group">
        <label for="user_email">User Email</label>
        <input class="form-control" value="<?php echo $user_email ?>" name="user_email" type="email">
    </div>
    <div class="form-group">
        <label for="user_password">Password</label>
        <input autocomplete="off" class="form-control" name="user_password" type="password">
    </div>
    <div class="form-group">
        <input class="btn btn-primary" type="submit" name="edit_user" value="Update User" >
    </div>
    

</form>