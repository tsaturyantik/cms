<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TsT</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/blog-home.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<?php

if(isset($_GET['lang']) && !empty($_GET['lang'])) {

    $_SESSION['lang'] = $_GET['lang'];

    if(isset($_SESSION['lang']) && $_SESSION['lang'] != $_GET['lang']){

        echo "<script type='text/javascript'> location.reload(); </script>";
    }
}

if(isset($_SESSION['lang'])){ 

    include "includes/languages/".$_SESSION['lang'].".php";
} else { 

    include "includes/languages/en.php";
}


?>


        <div class="container">

            <form method="get" class="navbar-form navbar-right" action="" id="language_form">
                <div class="form-group">
                    <select name="lang" class="form-control" onchange="changeLanguage()">
                        <option value="en" <?php if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'en'){echo "selected"; } ?> >English</option>
                        <option value="ru" <?php if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'ru'){echo "selected"; } ?>>Russian</option>
                    </select>
                </div>

            </form>
        </div>
        <section id="login">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3">
                <div class="form-wrap">
                <h1><?php echo _REGISTER; ?></h1>
                    <form role="form" action="" method="post" id="login-form" autocomplete="off">
                        <div class="form-group">
                            <label for="username" class="sr-only">username</label>
                            <input value="<?php echo isset($username) ? $username: ''?>" type="text" name="username" id="username" class="form-control" placeholder="Enter Desired Username" autocomplete="on">
                            <p><?php echo isset($error['username']) ? $error['username']: ''?></p>
                        </div>
                         <div class="form-group">
                            <label for="email" class="sr-only">Email</label>
                            <input value="<?php echo isset($user_email) ? $user_email: ''?>" type="email" name="email" id="email" class="form-control" placeholder="somebody@example.com" autocomplete="on">
                            <p><?php echo isset($error['email']) ? $error['email']: ''?></p>
                        </div>
                         <div class="form-group">
                            <label for="password" class="sr-only">Password</label>
                            <input type="password" name="password" id="key" class="form-control" placeholder="Password">
                            <p><?php echo isset($error['password']) ? $error['password']: ''?></p>
                        </div>
                
                        <input type="submit" name="register" id="btn-login" class="btn btn-primary btn-lg btn-block" value="Register">
                    </form>
                 
                </div>
            </div> <!-- /.col-xs-12 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section>

        <script>
            function changeLanguage(){

                document.getElementById("language_form").submit();
                // console.log('dadda');
            }
        </script>
    </body>
</html>