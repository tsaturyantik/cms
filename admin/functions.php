<?php 

//===== DATABASE HELPERS =====//

function redirect($location){
    header("Location:" . $location);
    exit;
}

function query($query){
    global $connection;
    return mysqli_query($connection, $query);
}

function fetchRecords($result){
    return mysqli_fetch_array($result);
}

function count_records($result){
    return mysqli_num_rows($result);
}

//===== END DATABASE HELPERS =====//

//===== GENERAL HELPER FUNCTIONS =====//

function get_user_name(){

    if(isset($_SESSION['username'])){
        return $_SESSION['username'];
    }
}

//===== END GENERAL HELPER FUNCTIONS =====//

//===== AUTHENTICATION HELPER FUNCTIONS =====//

function is_admin(){

    global $connection;

    if(isLoggedIn()){

        $query = "SELECT user_role FROM users WHERE user_id = ".$_SESSION['user_id']." ";
        $result = mysqli_query($connection, $query);
        confirmQuery($result);

        $row = mysqli_fetch_array($result);

        if($row['user_role'] == 'admin'){
            return true;
        } else {
            return false;
        }
    }
}

//===== END AUTHENTICATION =====//

// ===== USER SPECIFIC HELPERS =====//

function get_all_user_posts(){
    return query("SELECT * FROM posts WHERE user_id=".loggedInUserId()."");
}

function get_all_posts_user_comments(){
    return query("SELECT * FROM posts INNER JOIN comments ON posts.post_id = comments.comment_post_id WHERE user_id=".loggedInUserId()."");
}

function get_user_categories(){
    return query("SELECT * FROM categories WHERE user_id=".loggedInUserId()."");
}

function findALlCategoriesByUserId(){

    global $connection;
    $query = "SELECT * FROM categories WHERE user_id=".loggedInUserId()."";
    $select_categories = mysqli_query($connection, $query);

       while($row = mysqli_fetch_assoc($select_categories)){
       $cat_id = $row['cat_id'];
       $cat_title = $row['cat_title'];
       
       echo "<tr>";
       echo "<td>{$cat_id}</td>";
       echo "<td>{$cat_title}</td>";
       echo "<td><a href='categories.php?delete={$cat_id}'>Delete</a></td>";
       echo "<td><a href='categories.php?edit={$cat_id}'>Edit</a></td>";
       echo "<tr>";
   }
}

function get_user_published_posts(){

        global $connection;
        $query = "SELECT * FROM posts WHERE post_status = 'published' AND user_id=".loggedInUserId()."";
        $result = mysqli_query($connection, $query);
        confirmQuery($result);
        return mysqli_num_rows($result);
}

function get_user_draft_posts(){

    global $connection;
    $query = "SELECT * FROM posts WHERE post_status = 'draft' AND user_id=".loggedInUserId()."";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);
    return mysqli_num_rows($result);
}
function get_user_unapproved_comments(){

    global $connection;
    $query = "SELECT * FROM comments WHERE comment_status = 'unapproved' AND comment_user_id=".loggedInUserId()."";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);
    return mysqli_num_rows($result);
}

// ===== END USER SPECIFIC HELPERS =====//

function currentUser(){

    if($_SESSION['username']){
        return $_SESSION['username'];
    } 
        return false;
}

function imagePlaceHolder($image=''){

    if(!$image){
     echo "../images/no-image-available-png-4.png";
    }else{
        return $image;
    }
} 

function ifItIsMethod($method=null){

    if($_SERVER['REQUEST_METHOD'] == strtoupper($method)){
        return true;
    } 
        return false;
} 

function isLoggedIn(){

    if(isset($_SESSION['user_role'])){
        return true;
    }
    return false;
}

function loggedInUserId(){ 

    if(isLoggedIn()){
        $result = query("SELECT * FROM users WHERE username ='" . $_SESSION['username'] ."'");
        confirmQuery($result);
        $user = mysqli_fetch_array($result);
        return mysqli_num_rows($result) >= 1 ? $user['user_id']: false;
    }
    return false;
}

function userLikedThisPost($post_id = ''){
    $result = query("SELECT * FROM likes WHERE user_id = " .loggedInUserId() . " AND post_id = $post_id");
    confirmQuery($result);
    return mysqli_num_rows($result) >= 1 ? true: false;
}
 
function checkIfUserIsLoggedInAndRedirect($redirectLocation=null){
    if(isLoggedIn()){
        redirect($redirectLocation); 
    }
}

function users_online(){

    if(isset($_GET['onlineusers'])){
        global $connection;

        if(!$connection){
            session_start();
            include("../includes/db.php");

            $session = session_id();
            $time = time();
            $time_out_in_seconds = 03;
            $time_out = $time - $time_out_in_seconds;

            $query = "SELECT * FROM users_online WHERE session = '$session' ";
            $send_query = mysqli_query($connection, $query);
            $count = mysqli_num_rows($send_query);

                if($count == NULL){
                    mysqli_query($connection, "INSERT INTO users_online(session, time) VALUES('$session', '$time') ");
                } else {
                    mysqli_query($connection, "UPDATE users_online SET time = '$time' WHERE session = '$session'" );
                }

            $users_online_query = mysqli_query($connection, "SELECT * FROM users_online WHERE time > '$time_out'");
            echo $count_user = mysqli_num_rows($users_online_query);
            }
    } // get request isset
}

function confirmQuery($result){

    global $connection;
    if(!$result){
        die('QUERY FAILED' . mysqli_error($connection));
    }
}

function insert_categories(){

    global $connection;

    if(isset($_POST['submit'])){
        $cat_title = $_POST['cat_title'];
        $user_id = $_SESSION['user_id'];

        if($cat_title == "" || empty($cat_title)){
            echo "<div class='alert alert-danger' role='alert'> The Field Is Required!</div>";
        } else{
            echo "<div class='alert alert-success' role='alert'>Category Added Successfully!</div>";
            return query("INSERT INTO categories (cat_title,user_id) VALUES('$cat_title','$user_id')");
        }
    }
}

function findALlCategories(){

    global $connection;
    $query = "SELECT * FROM categories";
    $select_categories = mysqli_query($connection, $query);

       while($row = mysqli_fetch_assoc($select_categories)){
       $cat_id = $row['cat_id'];
       $cat_title = $row['cat_title'];
       
       echo "<tr>";
       echo "<td>{$cat_id}</td>";
       echo "<td>{$cat_title}</td>";
       echo "<td><a href='categories.php?delete={$cat_id}'>Delete</a></td>";
       echo "<td><a href='categories.php?edit={$cat_id}'>Edit</a></td>";
       echo "<tr>";
   }
}

function deleteCategories(){

    global $connection;

    if(isset($_GET['delete'])){
        $the_cat_id = $_GET['delete'];
        $query = "DELETE FROM categories WHERE(cat_id = '{$the_cat_id}')";
        $delete_query = mysqli_query($connection, $query);
        header("Location: categories.php");
    }
}

function recordCount($table){

    global $connection;
    $query = "SELECT * FROM " . $table;
    $select_all_data = mysqli_query($connection, $query);
    $result = mysqli_num_rows($select_all_data);
    confirmQuery($result);
    return $result;
}

function checkStatus($table,$column,$status){

    global $connection;
    $query = "SELECT * FROM $table WHERE $column = '$status' ";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);
    return mysqli_num_rows($result);
}

function checkUserRole($table,$column,$role){

    global $connection;
    $query = "SELECT * FROM $table WHERE $column = '$role' ";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);
    return mysqli_num_rows($result);
}

function username_exist($username){

    global $connection;
    $query = "SELECT username FROM users WHERE username = '$username'";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);

    if(mysqli_num_rows($result) > 0){
        return true;
    } else {
        return false;
    }
}

function email_exist($email){

    global $connection;
    $query = "SELECT user_email FROM users WHERE user_email = '$email'";
    $result = mysqli_query($connection, $query);
    confirmQuery($result);

    if(mysqli_num_rows($result) > 0){
        return true;
    } else {
        return false;
    }
}

function user_register($username,$user_email,$user_password){

    global $connection;
    $username = mysqli_real_escape_string($connection,$username);
    $user_email = mysqli_real_escape_string($connection,$user_email);
    $user_password = mysqli_real_escape_string($connection,$user_password);
    $user_password = password_hash($user_password, PASSWORD_BCRYPT, array('cost' => 12));

    $query = "INSERT INTO users (username,user_email,user_password,user_role) ";
    $query .= "VALUE('$username','$user_email', '$user_password', 'subscriber' )";
    $user_register_query = mysqli_query($connection,$query);
    confirmQuery($user_register_query);
}

function user_login($username,$password){

    global $connection;

    $username = trim($username);
    $password = trim($password);
    $username = mysqli_real_escape_string($connection, $username);
    $password = mysqli_real_escape_string($connection, $password);
    
    $query = "SELECT * FROM users WHERE username = '{$username}' ";
    $select_user_query = mysqli_query($connection, $query);
    confirmQuery($select_user_query);
    
    while($row = mysqli_fetch_assoc($select_user_query)){ 
        $db_id = $row['user_id'];
        $db_username = $row['username'];
        $db_user_password = $row['user_password'];
        $db_user_firstname = $row['user_firstname'];
        $db_user_lastname = $row['user_lastname'];
        $db_user_role = $row['user_role'];
        
        if(password_verify($password, $db_user_password)){
            $_SESSION['user_id'] = $db_id;
            $_SESSION['username'] = $db_username;
            $_SESSION['firstname'] = $db_user_firstname;
            $_SESSION['lastname'] = $db_user_lastname;
            $_SESSION['user_role'] = $db_user_role;

            if(is_admin()){
                redirect("/admin/dashboard.php");
            }else {
                redirect("/admin");
            }
        } else {
            return false;
        }
    }
    return true;
}

?>