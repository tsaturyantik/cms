<?php 

    if(isset($_POST['create_user'])){
        
        $username = mysqli_real_escape_string($connection,$_POST['username']);
        $user_firstname = mysqli_real_escape_string($connection,$_POST['user_firstname']);
        $user_lastname = mysqli_real_escape_string($connection,$_POST['user_lastname']);

        $user_image = $_FILES['image']['name'];
        $user_image_temp = $_FILES['image']['tmp_name'];

        $user_email = $_POST['user_email'];
        $user_password = $_POST['user_password'];
        $user_role = $_POST['user_role'];
        $user_date = date('d-m-y');
        $user_password = password_hash($user_password, PASSWORD_BCRYPT, array('cost' => 10));

        move_uploaded_file($user_image_temp, "../images/$user_image");

        $query = "INSERT INTO users(username,user_password,user_firstname,user_lastname,user_email,user_image,user_role) ";
        $query .= "VALUE('{$username}','{$user_password}','{$user_firstname}', '{$user_lastname}', '{$user_email}','{$user_image}','{$user_role}' )";
        
        $create_user_query = mysqli_query($connection, $query);
        confirmQuery($create_user_query);
        echo "<div class='alert alert-success' role='alert'>User Added Successfully!</div>";
        echo "<h4><a href='users.php'>View Users</a></h4>" ;
        
    } 

?>                              

<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="user_firstname">First Name</label>
        <input class="form-control" name="user_firstname" type="text">
    </div>
    <div class="form-group">
        <label for="user_lastname">Last Name</label>
        <input class="form-control" name="user_lastname" type="text">
    </div>
    <div class="form-group">
        <select name="user_role" id="">
        <option value="subscriber">Select Options</option>
            <option value="admin">Admin</option> 
            <option value="subscriber">Subscriber</option>
        
        </select>
    </div>
    <div class="form-group">
        <label for="user_image">User Image</label>
        <input type="file" name="image">
    </div>
    <div class="form-group">
        <label for="username">Username</label>
        <input class="form-control" name="username" type="text">
    </div>
    <div class="form-group">
        <label for="user_email">User Email</label>
        <input class="form-control" name="user_email" type="email">
    </div>
    <div class="form-group">
        <label for="user_password">Password</label>
        <input class="form-control" name="user_password" type="password">
    </div>
    <div class="form-group">
        <input class="btn btn-primary" type="submit" name="create_user" value="Add User" >
    </div>
    
</form>